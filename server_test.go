package main

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/magiconair/properties/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestListApi(t *testing.T) {
	t.Run("get list items", func(t *testing.T) {
		request := httptest.NewRequest(http.MethodGet, "/list", nil)

		request.Header.Set("Accept", "application/json")

		responseRecorder := httptest.NewRecorder()

		expectedResponseBody := []string{"List1", "List2"}

		context := echo.New().NewContext(request, responseRecorder)

		_ = GetList(context)

		var actualResponseBody []string

		_ = json.Unmarshal(responseRecorder.Body.Bytes(), &actualResponseBody)

		actualContentType := responseRecorder.Header().Get(echo.HeaderContentType)

		assert.Equal(t, actualContentType, echo.MIMEApplicationJSONCharsetUTF8)
		assert.Equal(t, responseRecorder.Code, http.StatusOK)
		assert.Equal(t, actualResponseBody, expectedResponseBody)

	})
}
