package main

import (
	"fmt"
	"github.com/couchbase/gocb/v2"
	"time"
)

func main() {
	//getListFromCouchBase()
	server, _ := NewServer()
	server.Start()

	// post data $array = ["key"=>"value"]
	//data := map[string]string{"name": "list10"}
	//testUpsertData(data)
}

func testUpsertData(data map[string]string) {
	bucket := GetBucket()
	collection := bucket.DefaultCollection()
	upsertResult, err := collection.Upsert("todo10", data, &gocb.UpsertOptions{})
	if err != nil {
		panic(err)
	}
	fmt.Println(upsertResult.Cas())

}

func getListFromCouchBase() {
	bucket := GetBucket()
	err := bucket.WaitUntilReady(5*time.Second, nil)
	if err != nil {
		panic(err)
	}
	collection := bucket.DefaultCollection()

	getResult, err := collection.Get("todo1", &gocb.GetOptions{})
	if err != nil {
		panic(err)
	}

	var myContent interface{}
	if err := getResult.Content(&myContent); err != nil {
		panic(err)
	}
	fmt.Println(myContent)
}
