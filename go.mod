module learngolang/todo-api

go 1.16

require (
	github.com/couchbase/gocb/v2 v2.2.1 // indirect
	github.com/labstack/echo/v4 v4.2.1 // indirect
	github.com/magiconair/properties v1.8.4 // indirect
	github.com/tylerb/graceful v1.2.15
)
