package main

import (
	"github.com/couchbase/gocb/v2"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/tylerb/graceful"
)

// Server sdsdfsdghgfgsd
type Server struct {
	e *echo.Echo
}

// NewServer sdfsdfdfghdgsdfg
func NewServer() (*Server, error) {
	server := &Server{}
	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.Recover())

	server.e = e
	return server, nil
}

// Start dsfdsfsfsd
func (s *Server) Start() error {

	s.e.Server.Addr = ":8080"
	s.e.GET("/health", s.healthCheck)

	s.e.GET("/list", GetListFromCouchBase)

	s.e.POST("/add-to-list", postUpsertData)

	s.e.POST("/add-to-list-insert", postInsertData)

	s.e.GET("/get-item/:id", getItem)

	return graceful.ListenAndServe(s.e.Server, 10*time.Second)

}

func getItem(context echo.Context) error {
	bucket := GetBucket()
	collection := bucket.DefaultCollection()
	getResult, err := collection.Get(context.Param("id"), &gocb.GetOptions{})
	if err != nil {
		panic(err)
	}

	var myContent interface{}
	if err := getResult.Content(&myContent); err != nil {
		panic(err)
	}
	_ = context.JSON(http.StatusOK, myContent)
	return nil
}

type (
	data struct {
		Name string `json:"name"`
	}
)

func postUpsertData(ctx echo.Context) error {
	data := new(data)
_:
	ctx.Bind(data)
	insert := map[string]string{"name": data.Name}
	upsertData(insert)
	return ctx.JSON(http.StatusOK, data.Name)
}
func postInsertData(ctx echo.Context) error {
	data := new(data)
_:
	ctx.Bind(data)
	insert := map[string]string{"name": data.Name}
	insertData(insert)
	return ctx.JSON(http.StatusOK, data.Name)
}

func insertData(insert map[string]string) gocb.Cas {
	bucket := GetBucket()
	collection := bucket.DefaultCollection()
	upsertResult, err := collection.Insert("todo11", insert, nil)
	if err != nil {
		panic(err)
	}
	return upsertResult.Cas()
}

func upsertData(insert map[string]string) gocb.Cas {
	bucket := GetBucket()
	collection := bucket.DefaultCollection()
	upsertResult, err := collection.Upsert("todo10", insert, &gocb.UpsertOptions{})
	if err != nil {
		panic(err)
	}
	return upsertResult.Cas()

}

func (s *Server) healthCheck(ctx echo.Context) error {
	return ctx.NoContent(http.StatusOK)
}

// GetList dsfdsfsd
func GetList(ctx echo.Context) error {
	listItems := []string{"List1", "List2"}
	_ = ctx.JSON(http.StatusOK, listItems)
	return nil
}

func GetListFromCouchBase(ctx echo.Context) error {
	cluster := ConnectCluster()
	bucket := cluster.Bucket("todolist")

	err := bucket.WaitUntilReady(5*time.Second, nil)
	if err != nil {
		panic(err)
	}
	collection := bucket.DefaultCollection()

	getResult, err := collection.Get("todo1", &gocb.GetOptions{})
	if err != nil {
		panic(err)
	}

	var myContent interface{}
	if err := getResult.Content(&myContent); err != nil {
		panic(err)
	}
	_ = ctx.JSON(http.StatusOK, myContent)
	return nil
}

func ConnectCluster() *gocb.Cluster {
	cluster, err := gocb.Connect(
		"localhost",
		gocb.ClusterOptions{
			Username: "Administrator",
			Password: "password",
		})
	if err != nil {
		panic(err)
	}
	return cluster
}
func GetBucket() *gocb.Bucket {
	cluster := ConnectCluster()
	bucket := cluster.Bucket("todolist")
	return bucket
}
